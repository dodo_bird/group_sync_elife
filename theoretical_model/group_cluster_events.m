function EVENTSGR = group_cluster_events(t,EVENTS,plotting)
% Find clusters of individual events by filtering a series of events.

if size(EVENTS{1},1)<size(EVENTS{1},2)
    for c = 1:numel(EVENTS)
        EVENTS{c} = EVENTS{c}';
    end
end

x = t*0;
for c = 1:numel(EVENTS)
    if numel(EVENTS{c})>1
        % x = x + sum(t==EVENTS{c}',2);
        [~,id] = min(abs(t-EVENTS{c}'),[],1);
        x(id) = x(id) + 1;
    end
end

sr = round(1/mean(diff(t)));
b = normpdf(-(sr/3):(sr/3), 0, 4);
xf = filtfilt(b, 1, x);
[~,locs] = findpeaks(xf, t, 'MinPeakDistance', .3);
EVENTSGR = locs;

% For debugging purposes
if plotting==1
    color_vec=cool(numel(EVENTS));
    plot(t,xf,'linewidth',2);hold on
    for c = 1:numel(EVENTS)
        line([EVENTS{c} EVENTS{c}]',[EVENTS{c}*0 EVENTS{c}*0+1]','color',color_vec(c,:))
    end
    line([EVENTSGR EVENTSGR]',[EVENTSGR*0 EVENTSGR*0+1]','color','k','linewidth',2)
    hold off
end