function dydt = ydotKuramoto_glob(t,y)
%ydotKuramoto
% Kuramoto dynamics with pulse coupling in the form of a Gamma distribution
global w K N stim noise

z = exp(1i*y);  % Wrapping theta around the circle
y = angle(z);   % Phase, -pi to pi

[~,idx] = min(abs(stim.t - t));
pt      = stim.x(:, idx);
n       = noise.x(:, idx);

which_model = 1;
switch which_model
    case 0
        Y = repmat(y, 1, N);
        dydt = w + (K/N) * sum(sin(Y-Y'))' + n;         % original Kuramoto model
    case 1
        C = ~diag(ones(1,N));
        dydt = w - (K/N) * sum(C.*(pt*sin(y)'))' + n;   % pulse coupled model
end

end