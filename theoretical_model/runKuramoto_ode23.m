function [theta,t,te] = runKuramoto_ode23(n,k,sigma,gamma_omega,f0,fs,duration,plotting)

global w K N stim noise pulse

N = n;
K = k;

tspan = [0 duration];
t = tspan(1):1/fs:tspan(2);

% The drum pulse
a = 1.25;
b = 0.02;
pulse.fs = fs;
pulse.t = (0:1/fs:0.25);
pulse.x = gampdf(pulse.t,a,b);
pulse.x = pulse.x*(fs/sum(pulse.x)); % Area under the curve should be exaclty 1
pulse.N = numel(pulse.t);

% Intrinsic frequencies, plus some constraints on the distribution
% w0 = f0*2*pi;
% w = ((w0-w0*gamma_omega) + (w0*(1+gamma_omega)-(w0-w0*gamma_omega)).*rand(N,1))*2*pi;
w = f0*2*pi + randn(N,1)*2*pi*gamma_omega;

% Random initial condition
y0  = 2*pi*rand(N,1) - pi;

% Noise
noise.fs = fs;
noise.t = t;
noise.x = sigma*randn(N, numel(t));
noise.N = N;

% Events vector
stim.fs = fs;
stim.t = t;
stim.N = length(stim.t);
stim.x = zeros(N,stim.N);

% Run the simulation
options = odeset('MaxStep', 1/stim.fs, 'Events', @eventsk);
[t, y, tev, ~, iev] = ode23(@ydotKuramoto_glob, tspan, y0, options);

% Various forms of the output for inspection
z     = exp(1i*y);                  % Oscillation (analytic)
theta = angle(z);                   % Wrapped phase, -pi to pi
mf    = (1/N)*sum(z,2);             % Mean field (the order parameter)
r     = abs(mf);                    % Amplitude of the mean field
osc   = real(mf);

% Compute individual and group stddev (sdi and sdg)
te = cell(1,N);
for nx = 1:N
    te{nx} = tev(iev==nx)';
end

% If necessary to inspect visually each simulation run.
if plotting == 1
    figure(16346794)
    ax1 = subplot(2,1,1);
    plot(stim.t, stim.x);
    title('Drum Pulses');
    xlabel('time t')
    ylabel('Pulses');
    grid
    
    ax2 = subplot(2,1,2);
    plot(t, osc, t, r, 'k', 'LineWidth', 2)
    title('Mean Field & Order Parameter');
    xlabel('time t')
    ylabel('Amplitude');
    grid
    
    linkaxes([ax1 ax2], 'x'); zoom xon;

    
    figure(112335)
    plot(t,theta./pi*.4+(1:size(theta,2)),'-','linewidth',2)
    hold on
    line([min(t);max(t)].*ones(1,size(theta,2)),(1:size(theta,2)).*ones(2,1),'color','k');
    for n = 1:numel(te)
        line([te{n};te{n}],[te{n}*0+n-.5;te{n}*0+n+.5],'color','k');
    end
    hold off

    figure(112335+1)
    plot(t,real(exp(1i*y))./pi*.4+(1:size(y,2)),'-','linewidth',2)
    hold on
    line([min(t);max(t)].*ones(1,size(theta,2)),(1:size(theta,2)).*ones(2,1),'color','k');
    for n = 1:numel(te)
        line([te{n};te{n}],[te{n}*0+n-.5;te{n}*0+n+.5],'color','k');
    end
    hold off
end