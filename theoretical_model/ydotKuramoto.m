function dydt = ydotKuramoto(t,y,w,K,N,Pt,noise)
%ydotKuramoto
% Kuramoto dynamics with pulse coupling in the form of a Gamma distribution

z = exp(1i*y);  % Wrapping theta around the circle
y = angle(z);   % Phase, -pi to pi

[~,idx] = min(abs(Pt.t - t));
pt      = Pt.x(:, idx);
n       = noise.x(:, idx);

which_model = 1;
switch which_model
    case 0 % original Kuramoto model
        Y = repmat(y, 1, N); 
        dydt = w + (K/N) * sum(sin(Y-Y'))' + n;
    case 1 % field (full) coupling topology. pulse-coupled model.
        C = ~diag(ones(1,N));
        dydt = w - (K/N) * sum(C.*(pt*sin(y)'))' + n;
    case 2 % ring coupling topology. pulse-coupled model
        C = logical(circshift(eye(N),1)+circshift(eye(N),-1));
        dydt = w - (K/N) * sum(C.*(pt*sin(y)'))' + n;
end

end