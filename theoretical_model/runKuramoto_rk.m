function [theta,t,te] = runKuramoto_rk(N,K,sigma,gamma_omega,f0,fs,duration,plotting)

t = (0:1/fs:duration);

% The drum pulse
a = 1.25;
b = 0.02;
Gamma.fs = fs;
Gamma.t = (0:1/fs:0.25);
Gamma.x = gampdf(Gamma.t,a,b);
Gamma.x = Gamma.x*(fs/sum(Gamma.x)); % Area under the curve should be exaclty 1
Gamma.N = numel(Gamma.t);

% Intrinsic frequencies, plus some constraints on the distribution
%w = (w0/2/pi + .1*linspace(-1.5,1.5,N)')*2*pi;
%w0 = w0/2/pi;
%w = ((w0-w0*gamma_omega) + (w0*(1+gamma_omega)-(w0-w0*gamma_omega)).*rand(N,1))*2*pi;
w = f0*2*pi + randn(N,1)*2*pi*gamma_omega;
%w = (2+.1*linspace(-1.5,1.5,N)')*2*pi;
%w(w<(w0/2)) = w0/2;
%w(w>(w0*2)) = w0*2;

% Random initial condition
y0  = 2*pi*rand(N,1) - pi;

% Noise
noise.fs = fs;
noise.t = t;
noise.x = sigma*randn(N, numel(t));
noise.N = N;

% Events vector
pulses.fs = fs;
pulses.t = t;
pulses.N = length(pulses.t);
pulses.x = zeros(N,pulses.N);

% Run the simulation
[y,Ee,pulses.x] = ode3e(@ydotKuramoto, t, y0, w, K, N, pulses, noise, Gamma);
y = y';

% Various forms of the output for inspection
z     = exp(1i*y);                  % Oscillation (analytic)
theta = angle(z)';                  % Wrapped phase, -pi to pi
mf    = (1/N)*sum(z);               % Mean field (the order parameter)
r     = abs(mf);                    % Amplitude of the mean field
osc   = real(mf);
t     = t';

% Compute individual and group stddev (sdi and sdg)
te = cell(1,N);
for nx = 1:N
    te{nx} = t(logical(Ee(nx,:)));
end

% If necessary to inspect visually each simulation run.
if plotting == 1
    figure(16346794)
    ax1 = subplot(2,1,1);
    plot(t, pulses.x);
    title('Drum Pulses');
    xlabel('time t')
    ylabel('Pulses');
    grid
    
    ax2 = subplot(2,1,2);
    plot(t, osc, t, r, 'k', 'LineWidth', 2)
    title('Mean Field & Order Parameter');
    xlabel('time t')
    ylabel('Amplitude');
    grid
    
    linkaxes([ax1 ax2], 'x'); zoom xon;
end

if plotting == 2
    figure(112335)
    plot(t,theta./pi*.4+(1:size(theta,2)),'-','linewidth',2)
    hold on
    line([min(t);max(t)].*ones(1,size(theta,2)),(1:size(theta,2)).*ones(2,1),'color','k');
    for n = 1:numel(te)
        line([te{n};te{n}],[te{n}*0+n-.5;te{n}*0+n+.5],'color','k');
    end
    hold off

    figure(112335+1)
    plot(t,real(exp(1i*theta))./pi*.4+(1:size(theta,2)),'-','linewidth',2)
    hold on
    line([min(t);max(t)].*ones(1,size(theta,2)),(1:size(theta,2)).*ones(2,1),'color','k');
    for n = 1:numel(te)
        line([te{n};te{n}],[te{n}*0+n-.5;te{n}*0+n+.5],'color','k');
    end
    hold off
end