clear

%%  Simulation parameters
plotting = 0;   % Should we look at each simulated trial's raw data? (This must be 0 if you're running with parfor.)
ntrials  = 1e2; % How many trials to simulate in each condition?
duration = 100; % Timespan, duration of each trial, in seconds?
fs       = 3e2; % Numerical integration time step freq.

%% Conditions
Ns = [2 4 8]; % Number of units in the ensemble.
Ks = [0 8];   % Coupling strength.

%% Fixed parameters
sigma = 6;        % Phase noise standard deviation.
sigma_omega = .5; % SD of intrinsic frequencies in the ensemble.
f0 = 2;           % The center of the distribution of intrinsic freqs, Hz.

%% Loop through conditions and trials and collect the variability (CV)
REZ = cell(numel(Ns)*numel(Ks)*ntrials,1);
for n = 1:numel(Ns)
    for k = 1:numel(Ks)
        N = Ns(n);
        K = Ks(k);
        fprintf('N = %.0f, K = %.2f. ',N,K)
        counter_blockoftrials = (k-1)*ntrials + (n-1)*numel(Ks)*ntrials;
        %for tr = 1:ntrials
        parfor tr = 1:ntrials
            % Simulate one trial.
            %[THETA,t,EVENTS] = runKuramoto_euler(N,K,sigma,sigma_omega,f0,fs,duration,plotting);
            [THETA,t,EVENTS] = runKuramoto_rk(N,K,sigma,sigma_omega,f0,fs,duration,plotting);
            
            % [THETA,t,EVENTS] = runKuramoto_ode23(N,K,sigma,sigma_omega,f0,fs,duration,plotting);
            % (This one was called ...Adaptive).
            % To run this one, you need to replace the factory solver ode23.m deep in Matlab's installation
            % with ours' edited ode23.m which can be found here in the 'odes' folder. After backing up the 
            % factory ode23.m and replacing it, you also need to restart Matlab.
            
            if plotting == 1;pause;end
            
            % Collect the variability of each unit.
            rez = nan(numel(EVENTS),5);
            for c = 1:numel(EVENTS)
                rez(c,:) = [N K 0 mean(diff(EVENTS{c})) std(diff(EVENTS{c}))/mean(diff(EVENTS{c}))];
            end
            if any(isnan(rez(:,5)))
                % Very rarely the simulation will fail for some random 
                % initial conditions and return weird results. Skip these.
                continue
            end
            
            % Same definition of group-aggregate as for drumming trials.
            EVENTSGR = group_cluster_events(t,EVENTS,plotting)';
            if plotting==1;pause;end
            rezg = [N K 1 mean(diff(EVENTSGR)) std(diff(EVENTSGR))/mean(diff(EVENTSGR))];
            
            REZ{tr + counter_blockoftrials} = [rez;rezg];
        end
        fprintf('Finished a batch of %.f%% of all trials.\n',1/numel(Ns)/numel(Ks)*100)
    end
end

temp = [];
for c = 1:numel(REZ)
    temp = vertcat(temp,REZ{c});
end
REZ = temp;

% Quickly summarize the outcomes.
MEANS = [];
for K = Ks
    for Agg = [0 1]
        for N = Ns
            index = all(REZ(:,1:3)==[N K Agg],2);
            MEANS = vertcat(MEANS,[N K Agg mean(REZ(index,5)) std(REZ(index,5)) std(REZ(index,5))./(sum(index)^.5)]);
        end
    end
end
bar(MEANS(:,4));hold on
errorbar(1:size(MEANS,1),MEANS(:,4),MEANS(:,6),'linestyle','none');hold off
set(gca,'XTickLabel',num2str(MEANS(:,[1 2 3])))
xtickangle(60)
ylabel('CV')

% Save the outcomes to a spreadsheet if it's necessary to analyze and display with R tools.
if 0
    REZ = array2table(REZ);
    REZ.Properties.VariableNames = {'N','K','Agg','ITI','cv'};
    writetable(REZ,['pulse_coupled_kuramoto_' datestr(now,'yyyymmdd-HHMMSS') '.csv'],'Delimiter',',')
end
