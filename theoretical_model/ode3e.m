function [Y,E,pulses] = ode3e(odefun,tspan,y0,varargin)
%ODE3E  Solve differential equations with a non-adaptive method of order 3.
%   Y = ODE3E(ODEFUN,TSPAN,Y0) with TSPAN = [T1, T2, T3, ... TN] integrates
%   the system of differential equations y' = f(t,y) by stepping from T0 to
%   T1 to TN. Function ODEFUN(T,Y) must return f(t,y) in a column vector.
%   The vector Y0 is the initial conditions at T0. Each row in the solution
%   array Y corresponds to a time specified in TSPAN.
%
%   Y = ODE3E(ODEFUN,TSPAN,Y0,P1,P2...) passes the additional parameters
%   P1,P2... to the derivative function as ODEFUN(T,Y,P1,P2...).
%
%   This is a non-adaptive solver. The step sequence is determined by TSPAN
%   but the derivative function ODEFUN is evaluated multiple times per step.
%   The solver implements the Bogacki-Shampine Runge-Kutta method of order 3.

if ~isnumeric(tspan)
    error('TSPAN should be a vector of integration steps.');
end

if ~isnumeric(y0)
    error('Y0 should be a vector of initial conditions.');
end

h = diff(tspan);
if any(sign(h(1))*h <= 0)
    error('Entries of TSPAN are not in order.')
end

try
    f0 = feval(odefun,tspan(1),y0,varargin{1:end-1});
catch ME
    msg = ['Unable to evaluate the ODEFUN at t0,y0. ',ME];
    error(msg);
end

y0 = y0(:);   % Make a column vector.
if ~isequal(size(y0),size(f0))
    error('Inconsistent sizes of Y0 and f(t0,y0).');
end

neq = length(y0);
N = length(tspan);
Y = zeros(neq,N);
F = zeros(neq,3);
E = false(neq,N);

Y(:,1) = y0;
for i = 2:N
    ti = tspan(i-1);
    hi = h(i-1);
    yi = Y(:,i-1);
    F(:,1) = feval(odefun,ti,yi,varargin{1:end-1});
    F(:,2) = feval(odefun,ti+0.5*hi,yi+0.5*hi*F(:,1),varargin{1:end-1});
    F(:,3) = feval(odefun,ti+0.75*hi,yi+0.75*hi*F(:,2),varargin{1:end-1});
    Y(:,i) = yi + (hi/9)*(2*F(:,1) + 3*F(:,2) + 4*F(:,3));
    
    % EWL 01/04/2022: This is the part that detects events 
    % and converts them to pulse functions.
    % 1. Detect events as zero-crossings in phase.
    ynow = angle(exp(1i*Y(:,(i-1):i)));
    for n = 1:size(ynow,1)
        if diff(mod(ynow(n,:),2*pi))<-pi % wraps at 2*pi.
            if (tspan(i)-max([0,max(tspan(E(n,:)))]))>.2 % the added noise can cause spurious bursts of zero-crossings
                E(n,i) = true;
            end
        end
    end
    % 2. If there is at least one event
    if sum(E(:,i)>0)
        ie = find(E(:,i) > 0);
        % 3. Add each event to the events vector
        for ix = ie'
            Nmin = min(i+varargin{end}.N-1, varargin{end-2}.N);
            varargin{end-2}.x(ix,i:Nmin) = varargin{end}.x(1:length(i:Nmin));
        end
    end
end
Y = Y.';
pulses = varargin{end-2}.x;