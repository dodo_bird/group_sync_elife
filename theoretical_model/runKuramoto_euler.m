function [THETA,t,EVENTS,OMEGA0]=runKuramoto_euler(N,K,sigma,gam,f0,sr,duration,plotting)
%sim_pulse_kuramoto_euler
% Numerically integrate Ed Large and Lana Delasanta's pulse-coupled Kuramoto model.
% The input parameters should be self-explanatory. sigma is the phase noise
% standard deviation, gamma is the width of the distribution of intrinsic
% frequencies, centered at omega_center. 
% Set plotting to one to see the raw data.
%
% Dobri Dotov, 2021, Hamilton, Ontario

start = 0;
stop = start + duration; % seconds
dt = 1/sr;
len = round(duration/dt);
t = linspace(start,stop,len)';

% Pulse (Gamma) function and parameters.
a = 1.25;
b = .02;
Pt = @(t,a,b)(1./(b.^a.*gamma(a)).*t.^(a-1).*exp(-t./b));

% Intrinsic frequencies. Assume we'll be working in the positive range.
OMEGA0 = -inf;
while any(OMEGA0<0)
    OMEGA0 = f0*2*pi + randn(1,N)*2*pi*gam;
    % OMEGA0 = omega_center/gam + (omega_center*gam-omega_center/gam).*rand(1,N)*2*pi;
end
% disp(OMEGA0)

% Empty arrays.
events = zeros(1,N);
EVENTS = cell(1,N);
THETA = nan(len,N);

% Random initial conditions.
theta = (randi(N,1,N)./N)*2*pi;
THETA(1,:) = theta;

% Start the show.
for c = 2:len
    Pulse = Pt((t(c)-events).*((t(c)-events)>0),a,b);
    theta = theta + dt*OMEGA0 + dt*randn(1,size(theta,2))*sigma - dt*K/N.*Pulse*sin(theta.*(~eye(numel(theta))));
    THETA(c,:) = theta;
    
    % Detect events as a zero-crossing in phase.
    for n = 1:N
        if diff(mod(THETA(c-1:c,n),2*pi))<-pi % wraps at 2*pi.
            if (t(c)-events(n))>.2 % the added noise can cause spurious bursts of zero-crossings
                events(n) = t(c);
                EVENTS{n} = vertcat(EVENTS{n},events(n));
            end
        end
    end
end

% Display the raw data.
if plotting == 1
    figure(112335)
    plot(t,THETA./pi*.4+(1:size(THETA,2)),'-','linewidth',2)
    hold on
    line([min(t);max(t)].*ones(1,size(THETA,2)),(1:size(THETA,2)).*ones(2,1),'color','k');
    for n = 1:numel(EVENTS)
        line([EVENTS{n}';EVENTS{n}'],[EVENTS{n}'*0+n-.5;EVENTS{n}'*0+n+.5],'color','k');
    end
    hold off

    figure(112335+1)
    plot(t,real(exp(1i*THETA))./pi*.4+(1:size(THETA,2)),'-','linewidth',2)
    hold on
    line([min(t);max(t)].*ones(1,size(THETA,2)),(1:size(THETA,2)).*ones(2,1),'color','k');
    for n = 1:numel(EVENTS)
        line([EVENTS{n}';EVENTS{n}'],[EVENTS{n}'*0+n-.5;EVENTS{n}'*0+n+.5],'color','k');
    end
    hold off
end
