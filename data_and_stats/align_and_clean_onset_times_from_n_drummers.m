function G = align_and_clean_onset_times_from_n_drummers(TIMES,varargin)
%ALIGN_AND_CLEAN_ONSET_TIMES_FROM_N_DRUMMERS
% This script shows one possible procedure for pairing onset times from
% several individuals, say drummers, and obtaining the asynchronies among
% them. Importantly, the basic assumption is that the participants are
% performing the task reasonably well and their onset times are clustered 
% close to each other. This script should be useful if you expect there to
% be a few missed and misaligned taps and nothing more. If participants are
% strongly diverging, for example because they are not even tapping at the 
% same tempo, then the outputs of this script won't have any meaning. 
% This was tested on an isochronous tapping task. 
% Use with care. The pipeline really depends on the task, the tempo, how 
% well the participants synchronize, etc..
% 
% INPUT
%   TIMES: Each cell is a column vector with onset times in seconds.
%   the second argument is a plotting flag, to visualize the process or no?
%   the 3rd and 4rd arguments you can leave empty, unless you want to 
%   experiment with rhythms, not just isochronous tapping.
%
% OUTPUT
%   G: a structure with an m-by-n matrix of aligned, paired, and cleaned 
%   inter-onset intervals as tempos, plus other variables such as the
%   asynchronies among the onset times.
% 
% Try the following:
% clear;close all
% async_std = .01;
% TIMES{1}=(1:1:100)';
% for c=2:4
%     TIMES{c}=randperm(numel(TIMES{1}))';
%     TIMES{c}=sort(TIMES{c}(1:end-10));
%     TIMES{c}=TIMES{c}+randn(size(TIMES{c}))*async_std;
% end
% G = align_and_clean_onset_times_from_n_drummers(TIMES,1);
% [async_std std(reshape(G.async(:,1:4),[],1))]
%
% The observed asynchrony SD should be similar to the theoretical despite 
% there being complicating missed taps.
%
% Dobri Dotov, 2022


if numel(varargin)>0
    plotting = varargin{1};
else
    plotting = 1;
end
% You might have some luck with rhythms where the inter-onset-intervals 
% fall in a small number of clusters. This hasn't been tested extensively.
if numel(varargin)>1
    n_clusters = varargin{2};
else
    n_clusters = 1;
end
if numel(varargin)>2 % Help by guessing what are the intervals.
    starting_clusters = varargin{3};
else
    starting_clusters = [];
end

% Align the onset times by computing a common reference which is their
% local central tendency within some window.
X=[];
bpm=0; % A crude measure of tempo.
for c=1:numel(TIMES)
    X = [X;TIMES{c}];
    bpm = bpm + 60*1/mean(diff(TIMES{c}));
end
bpm = bpm/c;
y = virtual_reference(X,numel(TIMES),plotting,bpm);
TIMES{end+1}=y; % Don't forget that the last column of TIMES is the ref.


% Pair individual onset times with the closest reference onset time.
cols_remove = [];
xt=nan(numel(TIMES{end})-2,size(TIMES,2));
xt(:,end)=TIMES{end}(2:end-1);
for c = 1:size(TIMES,2)-1
    if ~isempty(TIMES{c})
        for r=1:size(xt,1)
            [~,ind] = min((xt(r,end)-TIMES{c}).^2);
            xt(r,c) = TIMES{c}(ind);
        end
    else % Clean empty channels.
        cols_remove = [cols_remove c];
    end
end
xt(:,cols_remove) = [];


% Remove all nan-rows and repeats
dxt=diff(xt);
index1 = sum(isnan(dxt),2)>0;
index2 = sum(dxt==0,2)>0;
xt(find(index1 | index2)+1,:)=[];
dxt(index1 | index2,:)=[];
dxt = 60./dxt;


% (Experimental) Flatten the rhythm by using clustering, so that you can 
% remove bad onset times equally fairly for short and long intervals.
idx=zeros(size(dxt));
C = [];
for c=1:size(dxt,2)
    if isempty(starting_clusters)
        [idx(:,c),C(c,:)]=kmeans(log(dxt(:,c)),n_clusters);
    else
        if n_clusters == 1 && numel(starting_clusters)==1
            starting_clusters = median(dxt(:,c));
        end
        [idx(:,c),C(c,:)]=kmeans(log(dxt(:,c)),n_clusters,'Start',log(starting_clusters));
    end
end
C = exp(C);
higher_cutoff = 1.3*max(C,[],2)';
lower_cutoff = .7*min(C,[],2)';

dxt_flat = dxt;
if n_clusters==3 % Only tried this with rhythms made of short, mid, and long interval.
    for c=1:size(dxt_flat,2)
        dxt_flat(idx(:,c)==1,c) = dxt_flat(idx(:,c)==1,c).*2;
        dxt_flat(idx(:,c)==3,c) = dxt_flat(idx(:,c)==3,c)./2;
    end
end

if plotting == 1
    figure
    for c=1:numel(TIMES)
        line(TIMES{c}'.*[1 1]',TIMES{c}'.*[0 0]'+[c-1 c]','color','k')
    end
    figure
    for c=1:numel(TIMES)
        subplot(numel(TIMES),1,c)
        plot(60./diff(TIMES{c}),'-ok')
    end
    figure
    for c=1:size(dxt,2)
        subplot(size(dxt,2),1,c)
        plot(xt(2:end,c),dxt(:,c),'-k')
        hold on
        for n=1:n_clusters
            plot(xt([false;idx(:,c)==n],c),dxt(idx(:,c)==n,c),'o')
        end
        hold off
    end
end


% Crop skipped beats and such.
index = (sum(dxt_flat<lower_cutoff,2)==0) & (sum(dxt_flat>higher_cutoff,2)==0);
idx = idx(index,:);
dxt_flat = dxt_flat(index,:);
xt = xt(find(index)+1,:);

if plotting == 1
    figure
    for c=1:size(dxt_flat,2)
        subplot(size(dxt_flat,2),1,c)
        plot(xt(:,c),dxt_flat(:,c),'-k')
        hold on
        for n=1:n_clusters
            plot(xt(idx(:,c)==n,c),dxt_flat(idx(:,c)==n,c),'o')
        end
        hold off
    end
end


% Output and some stats of the asynchronies.
G.t=xt;
G.tempos = dxt_flat;
G.note_indices = idx;
G.async = xt-xt(:,end);
G.async(abs(G.async)>.3) = nan;
G.masync = nanmean(G.async);
G.sdasync = nanstd(G.async);

end

function y = virtual_reference(X,n,plotting,bpm)
% Take in a vector of onset times, project to a binary vector of events in
% 1e3-Hz time, then convolve with a Gaussian kernel and find the clusters.

sr=1000; % Hz
winlen=200; % ms
xf=zeros(round((X(end)+1)*sr),1);
t=(1:numel(xf))'./sr;
for x=1:numel(X) % In case there are exact matches, or it would be lost for the sync measures.
    xf(round(X(x)*1e3)+1)=xf(round(X(x)*1e3)+1)+1;
end
kern=gausswin(round(sr/1e3*winlen));
activity_rate = conv(xf,kern,'same')./n;
xfc=smooth(activity_rate,round((60/bpm*sr)/4));
[xfcp,xfcpin]= findpeaks(xfc,'MinPeakDistance',round((60/bpm*sr)/3));
y=t(xfcpin);
activity_rate = [t(xfcpin) activity_rate(xfcpin)];

if plotting
    figure
    plot(t,[xf,xfc],'LineWidth',.5)
    hold on
    plot(t(xfcpin),xfcp,'or','LineWidth',1.)
    plot(activity_rate(:,1),activity_rate(:,2),'pm','LineWidth',1.)
    hold off
end

end