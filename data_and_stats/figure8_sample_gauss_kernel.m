function figure_sample_gauss_kernel()

if false
    [alw,fsz,lw,msz] = pretty_canvas([4*5/4 3*5/4]);
else
    alw = 1;fsz = 8;lw=2;msz=5;
end

bpm = 80;
sr = 1e3;
conv_win_len_ms = 200;
embargo_win = 200; % ms %round((60/bpm*sr)/3);
embargo_win = round(embargo_win/1e3*sr); % samples
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr));
n=8;
t=(9.8:(1/sr):11.9)';
x=zeros(numel(t),n);
for beats=10.5:(60/bpm):11.8
    for k_ind=1:n
        x(1*randi(round(sr*(.02+(beats-10)*2/10)),1)'+find(t==beats),k_ind)=1;
    end
end
pop_act=conv(sum(x,2),gaussFilter,'same')./n;
[pks,locs]=findpeaks(pop_act,'MinPeakDistance',embargo_win);
clf
for k_ind=1:n
    st = t(x(:,k_ind)==1);
    line([st st]',st'.*[0;0]+[0;1],'color','k','linewidth',lw-1)
end
line(t(locs)'.*[1;1],[pks*0 pks]','color','r','linewidth',lw);
hold on
l(1)=plot(t,t*0,'-k','linewidth',lw-1.);
l(2)=plot(t(2e2+1:5:2e2+200),gaussFilter(1:5:end).*.6+.4,'-.k','linewidth',lw);
l(3)=plot(t,pop_act,'--k','linewidth',lw);
l(4)=plot(t(locs),pop_act(locs),'or','linewidth',lw,'markersize',msz,'markerfacecolor','r');
legend(l,{'Drummers'' onsets','Convolution kernel','Activity rate','Group-aggregate onsets'},'fontsize',fsz)
hold off
xlabel('Time, s','fontsize',fsz+2)
ylim([-.1 1.1])
xlim([min(t) max(t)])
set(gca,'YTick',[])
set(gca,'linewidth',alw)

return

% print('-djpeg','-r600',['fig_conv_gauss_sample_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpg'])

function [alw,fsz,lw,msz] = pretty_canvas(dims)

if isempty(dims)
    width = 4; % "
    height= 3.; % "
else
    width = dims(1);
    height= dims(2);
end

alw = .75;    % AxesLineWidth
fsz = 10;      % Fontsize
lw = 2;      % LineWidth
msz = 7;       % MarkerSize

% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;

% The properties we've been using in the figures
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz

% Set the default Size for display
defpos = get(0,'defaultFigurePosition');
set(0,'defaultFigurePosition', [defpos(1) defpos(2) width*100, height*100]);

% Set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','inches'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);

