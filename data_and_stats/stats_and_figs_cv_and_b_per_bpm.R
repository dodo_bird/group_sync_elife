library(lme4)
library(lmerTest)
library(effectsize)
library(texreg)
library(plyr)
library(ggplot2)
library(wesanderson)
source('multiplot.R')

# setwd('...')
X<-read.csv('trial_cv_and_b_plus.csv',sep=',')

# Unique person and group ids.
X$Group <- X$N*1e4 + X$Group*1e2
X$ID <- factor(X$Group + X$PP)
X$Group <- factor(X$Group)
xtabs(~Group,data=X)

X$GroupAggr <- (X$PP==10)*1

# Separate the impossible tempos
index_remove <- array(0,dim=dim(X)[1])
for (b in c(50,100,240)){
  index_remove = index_remove + 1*(X$BPM==b)
}
X2<-X[index_remove==1,]
X<-X[index_remove==0,]

# Select only the individuals in Ensemble, SCT condition
x2<-X2[(X2$SCT==1) & (X2$Coupled==1) & (X2$GroupAggr==0),] 
x<-X[(X$SCT==1) & (X$Coupled==1) & (X$GroupAggr==0),] 

# Figure 4 supplement 1A
m3=lmer(b ~ 1 + BPM + N:BPM + (1|ID),data=x,REML=0)
x$fitted <- getME(m3,'X') %*% fixef(m3)

colour_palette <- wes_palette("FantasticFox1",3,type=("continuous"))
g<-list('vector',2)
g[[1]]<-
  ggplot(x, aes(x=BPM, y=b, color=as.factor(N), fill=as.factor(N))) +
    geom_jitter(size=1, alpha=.5, width=5, height=.0) +
    geom_jitter(data=x2, aes(x=BPM, y=b, color=as.factor(N), fill=as.factor(N)), size=1, alpha=.3, width=5, height=.0) +
    geom_line(data=x, aes(x=BPM, y=fitted, colour=as.factor(N)), size=1.5, alpha=.7) +
    theme_classic() +
    theme(panel.background = element_rect(fill = "#eeeeee",
                                          colour = "#000000", size = .1, linetype = "solid")) +
    theme(legend.position="top", legend.title=element_blank()) +
    scale_colour_manual(values=colour_palette) +
    scale_x_continuous(breaks=c(50,80,100,120,160,200,240)) +
    labs(y = expression(paste('Speeding up, ','minute'^{-1}))) +
    labs(x = 'Stimulus tempo, bpm') +
    theme(axis.text.x = element_text(size=14)) +
    theme(axis.text.y = element_text(size=14)) +
    theme(axis.title.x = element_text(size=14)) +
    theme(axis.title.y = element_text(size=14)) +
    annotate("text", label = paste('A'), x = 240, y = .2, size=6)


# Figure 4 Supplement 1B
m3=lmer(cv ~ 1 + N+BPM + (1|Group),data=x,REML=0)
x$fitted <- getME(m3,'X') %*% fixef(m3)
g[[2]]<-
  ggplot(x, aes(x=BPM, y=cv, color=as.factor(N), fill=as.factor(N))) +
    geom_jitter(size=1, alpha=.5, width=5, height=.0) +
    geom_jitter(data=x2, aes(x=BPM, y=cv, color=as.factor(N), fill=as.factor(N)), size=1, alpha=.3, width=5, height=.0) +
    geom_line(data=x, aes(x=BPM, y=fitted, colour=as.factor(N)), size=1.5, alpha=.7) +
    theme_classic() +
    theme(panel.background = element_rect(fill = "#eeeeee",
                                          colour = "#000000", size = .1, linetype = "solid")) +
    theme(legend.position="top", legend.title=element_blank()) +
    scale_colour_manual(values=colour_palette) +
    scale_x_continuous(breaks=c(50,80,100,120,160,200,240)) +
    labs(y = 'CV') +
    labs(x = 'Stimulus tempo, bpm') +
    theme(axis.text.x = element_text(size=14)) +
    theme(axis.text.y = element_text(size=14)) +
    theme(axis.title.x = element_text(size=14)) +
    theme(axis.title.y = element_text(size=14)) +
    annotate("text", label = paste('B'), x = 240, y = .15, size=6)

multiplot(plotlist=g,cols=2)

if (FALSE){
  filename=paste("fig4s1_b_and_cv_per_bpm_",Sys.Date(),'.png',sep='')
  png(filename=filename,width=8,height=4,units="in",res=300)
  multiplot(plotlist=g,cols=2)
  dev.off()
}

# For figure source data file:
# library(plyr)
# s <- ddply(rbind(x[,seq(1,dim(x)[2]-1)],x2), c('BPM','N'), summarise, mean=mean(b), sem=sd(b)/sqrt(length(b)), n=length(b))
# write.csv(s,paste('fig_summary_stats',format(Sys.time(),"%x-%H%M%S"),'.csv',sep=""),row.names=FALSE)
# s <- ddply(rbind(x[,seq(1,dim(x)[2]-1)],x2), c('BPM','N'), summarise, mean=mean(cv), sem=sd(cv)/sqrt(length(cv)), n=length(cv))
# write.csv(s,paste('fig_summary_stats',format(Sys.time(),"%x-%H%M%S"),'.csv',sep=""),row.names=FALSE)

