The code here accompanies the paper "Collective dynamics support group drumming, reduce variability, and stabilize tempo drift" by Dotov, Delasanta, Cameron, Large, and Trainor, https://elifesciences.org/articles/74816.

The data folder includes data, R code for statistical analysis and figures, and sample Matlab code for processing and cleaning raw onset times.

The modeling folder contains all the necessary Matlab code to reproduce the dynamic systems theoretical account which consists of a pulse-coupled (or hybrid continuous-discrete) Kuramoto system.
